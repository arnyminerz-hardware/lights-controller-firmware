<h1 align="center">Lights Controller Firmware</h1>
<p align="center">For controlling a lights system with DIY elements<p>
<p align="center">
    <a href="https://gitlab.com/arnyminerz-hardware/lights-controller-firmware"><img src="https://img.shields.io/gitlab/pipeline/arnyminerz-hardware/lights-controller-firmware/master?style=for-the-badge" /></a>
    <a href="https://gitmoji.carloscuesta.me"><img src="https://img.shields.io/badge/gitmoji-%20😜%20😍-FFDD67.svg?style=for-the-badge" alt="Gitmoji"></a>
</p>

This is the firmware for the Lights Controller control board.

## Configuration
All the configuration can be made through the `pref.h` file. There are a lot of options you can adjust, which are well explained in the corresponding lines.

Note that all the times are in milliseconds, unless specified.

# Neotrellis
## Tiles Address Calculation
`0x2E + A4 + A3 + A2 + A1 + A0`
So, since we have soldered `A0+A1` for one, its address will be


# Power supply
The installed power supply has a nominal voltage of `9V`, so it's perfect for the VIN pin of the `ESP32 Devkit-V1`, which has an internal `AMS1117`, with a input voltage of between `7` and `9` volts.