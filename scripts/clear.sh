#!/bin/bash
# A file for clearing all the cached data generated for the certificate
# @author ArnyminerZ
# @version 2020/11/07

rm -f exampleca.* example.* cert.h private_key.h