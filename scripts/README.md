# `create-cert.sh`
Creates an https certificate for the web server
## Environment Variables
### `CERT_COMPANY_NAME`
**Required**

**Description:** The name of the company for the certificate

**Example:** `MyCompany`
### `CERT_COUNTRY_CODE`
**Required**

**Description:** The country code where the company is based

**Example:** `ES`
### `CERT_STATE`
**Required**

**Description:** The state code inside the country where the company is based

**Example:** `Alacant`
### `CERT_CITY`
**Required**

**Description:** The name of the city where the company is based

**Example:** `Alcoi`
### `CERT_DNS`
**Required**

**Description:** The DNS name for the certificate. You may use `.local` domain.

**Example:** `mydevice.local`