#!/bin/bash
# A file for generating the https certificates for the server
# @author fhessel,ArnyminerZ
# @version 2020/11/07

echoerr() { echo "$@" 1>&2; } # For logging errors. Credit: https://stackoverflow.com/a/2990533

error=0
# Get all the environment variables
if [[ -z ${CERT_COMPANY_NAME} ]]; then
    echoerr "CERT_COMPANY_NAME environment variable was not set"
    error=1
else
    CNAME="$CERT_COMPANY_NAME"
fi

if [[ -z ${CERT_COUNTRY_CODE} ]]; then
    echoerr "CERT_COUNTRY_CODE environment variable was not set"
    error=1
else
    COUNTRY_CODE="$CERT_COUNTRY_CODE"
fi

if [[ -z ${CERT_CITY} ]]; then
    echoerr "CERT_CITY environment variable was not set"
    error=1
else
    CITY="$CERT_CITY"
fi

if [[ -z ${CERT_STATE} ]]; then
    echoerr "CERT_STATE environment variable was not set"
    error=1
else
    STATE="$CERT_STATE"
fi

if [[ -z ${CERT_DNS} ]]; then
    echoerr "CERT_DNS environment variable was not set"
    error=1
else
    DNS="$CERT_DNS"
fi

if [[ "$error" -eq 1 ]]; then
    exit 1
fi

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CERTS_DIR="$SCRIPT_DIR/../include/cert"

set -e
#------------------------------------------------------------------------------
# cleanup any previously created files
rm -f exampleca.* example.* cert.h private_key.h

#------------------------------------------------------------------------------
# create a CA called "myca"

# create a private key
openssl genrsa -out exampleca.key 1024

# create certificate
cat > exampleca.conf << EOF  
[ req ]
distinguished_name     = req_distinguished_name
prompt                 = no
[ req_distinguished_name ]
C = DE
ST = BE
L = Berlin
O = MyCompany
CN = myca.local
EOF
openssl req -new -x509 -days 3650 -key exampleca.key -out exampleca.crt -config exampleca.conf
# create serial number file
echo "01" > exampleca.srl

#------------------------------------------------------------------------------
# create a certificate for the ESP (hostname: "myesp")

# create a private key
openssl genrsa -out example.key 1024
# create certificate signing request
cat > example.conf << EOF  
[ req ]
distinguished_name     = req_distinguished_name
prompt                 = no
[ req_distinguished_name ]
C = $COUNTRY_CODE
ST = $STATE
L = $CITY
O = $CNAME
CN = $DNS
EOF
openssl req -new -key example.key -out example.csr -config example.conf

# have myca sign the certificate
openssl x509 -days 3650 -CA exampleca.crt -CAkey exampleca.key -in example.csr -req -out example.crt

# verify
openssl verify -CAfile exampleca.crt example.crt

# convert private key and certificate into DER format
openssl rsa -in example.key -outform DER -out example.key.DER
openssl x509 -in example.crt -outform DER -out example.crt.DER

# create header files
echo "#ifndef CERT_H_" > ./cert.h
echo "#define CERT_H_" >> ./cert.h
xxd -i example.crt.DER >> ./cert.h
echo "#endif" >> ./cert.h

echo "#ifndef PRIVATE_KEY_H_" > ./private_key.h
echo "#define PRIVATE_KEY_H_" >> ./private_key.h
xxd -i example.key.DER >> ./private_key.h
echo "#endif" >> ./private_key.h

# Create the certificates dir
mkdir -p "$CERTS_DIR"

echo "Copying generated certificate data..."
cp ./cert.h ./private_key.h "$CERTS_DIR"

echo ""
echo "Certificates created!"
echo "---------------------"
echo ""
echo "  Private key:      private_key.h"
echo "  Certificate data: cert.h"
echo ""
echo "  Path: $CERTS_DIR"
echo ""