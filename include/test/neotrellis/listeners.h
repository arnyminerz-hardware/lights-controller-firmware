/**
 * Those are callback functions for checking if Neotrellis
 *   is working properly.
 * @file listeners.h
 * @author ArnyminerZ
 * @date 2020/10/05
 */

#ifndef TEST_NT_LISTENERS_H
#define TEST_NT_LISTENERS_H

#include <Adafruit_NeoTrellis.h>

#include "helper/neotrellis.h"
#include "helper/encoder.h"

uint16_t pressedKeys = 0;

TrellisCallback check_press(keyEvent evt)
{
#ifndef DISABLE_NEOTRELLIS
    uint16_t index = evt.bit.NUM;

    if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING)
    {
        Serial.print("Pressed Neotrellis #");
        Serial.println(index);
        pressedKeys++;
        trellis_paint(index, 0, 255, 0); //on rising
    }

    trellis.unregisterCallback(index);
#endif

    return 0;
}

TrellisCallback trellis_routine(keyEvent evt)
{
#ifndef DISABLE_NEOTRELLIS
    uint16_t index = evt.bit.NUM;

    if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING)
    {
        if (index == 0 || index == 4 || index == 8 || index == 12)
        {
            trellisStatus[index][0] = trellisStatus[index][0] == 255 ? 0 : 255;
            trellisStatus[index][1] = 0;
            trellisStatus[index][2] = 0;

            trellisStatus[++index][0] = 0;
            trellisStatus[index][1] = 0;
            trellisStatus[index][2] = 0;

            trellisStatus[++index][0] = 0;
            trellisStatus[index][1] = 0;
            trellisStatus[index][2] = 0;

            trellisStatus[++index][0] = 0;
            trellisStatus[index][1] = 0;
            trellisStatus[index][2] = 0;
        }
        else if (index == 1 || index == 5 || index == 9 || index == 13)
        {
            trellisStatus[--index][0] = 0;
            trellisStatus[index][1] = 0;
            trellisStatus[index][2] = 0;

            trellisStatus[++index][0] = 0;
            trellisStatus[index][1] = trellisStatus[index][1] == 255 ? 0 : 255;
            trellisStatus[index][2] = 0;

            trellisStatus[++index][0] = 0;
            trellisStatus[index][1] = 0;
            trellisStatus[index][2] = 0;

            trellisStatus[++index][0] = 0;
            trellisStatus[index][1] = 0;
            trellisStatus[index][2] = 0;
        }
        else if (index == 2 || index == 6 || index == 10 || index == 14)
        {
            trellisStatus[----index][0] = 0;
            trellisStatus[index][1] = 0;
            trellisStatus[index][2] = 0;

            trellisStatus[++index][0] = 0;
            trellisStatus[index][1] = 0;
            trellisStatus[index][2] = 0;

            trellisStatus[++index][0] = 0;
            trellisStatus[index][1] = 0;
            trellisStatus[index][2] = trellisStatus[index][2] == 255 ? 0 : 255;

            trellisStatus[++index][0] = 0;
            trellisStatus[index][1] = 0;
            trellisStatus[index][2] = 0;
        }
        else if (index == 3 || index == 7 || index == 11 || index == 15)
        {
            trellisStatus[------index][0] = 0;
            trellisStatus[index][1] = 0;
            trellisStatus[index][2] = 0;

            trellisStatus[++index][0] = 0;
            trellisStatus[index][1] = 0;
            trellisStatus[index][2] = 0;

            trellisStatus[++index][0] = 0;
            trellisStatus[index][1] = 0;
            trellisStatus[index][2] = 0;

            trellisStatus[++index][0] = trellisStatus[index][0] == 255 ? 0 : 255;
            trellisStatus[index][1] = trellisStatus[index][1] == 255 ? 0 : 255;
            trellisStatus[index][2] = trellisStatus[index][2] == 255 ? 0 : 255;
        }
        else
        {
            Serial.print("Encoder value:");
            Serial.println((uint8_t) encoderValue);
            trellisStatus[index][0] = (uint8_t) encoderValue;
            trellisStatus[index][1] = 255;
            trellisStatus[index][2] = 0;
        }
    }
    if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_FALLING && index > 15)
    {
        trellisStatus[index][0] = 0;
        trellisStatus[index][1] = 0;
        trellisStatus[index][2] = 0;
    }

    for (int c = 0; c < NEOTRELLIS_COUNT; c++)
        trellis_paint(c, trellisStatus[c][0], trellisStatus[c][1], trellisStatus[c][2], false);

    trellis.show();
#endif

    return 0;
}

#endif