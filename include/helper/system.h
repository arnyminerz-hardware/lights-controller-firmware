/**
 * A helper file to make some system management
 *   easier.
 * @file system.h
 * @author ArnyminerZ
 * @date 2020/10/05
 */

#ifndef SYSTEM_H
#define SYSTEM_H

void reboot(){
    ESP.restart();
}

#endif