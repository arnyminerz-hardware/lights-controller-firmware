/**
 * A helper so preferences are easier to control
 * @file preferences.h
 * @author ArnyminerZ
 * @date 2020/10/05
 */

#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <Preferences.h>
#include <Arduino.h>
#include <string>

#include "static/pref.h" // For getting the namespace

using namespace std;

Preferences preferences; // The preferences storage instance

bool _prefOpen = false;

/**
 * Opens the preferences in rw mode so you can start
 *   writing and reading data.
 * @author ArnyminerZ
 * @version 1 2020/10/05
 */
bool pref_open()
{
    if (_prefOpen)
        return true;
    bool beginned = preferences.begin(PREF_NAMESPACE, false); // false is for read-write mode.
    _prefOpen = beginned;
    return beginned;
}

/**
 * Closes the preference access once all the tasks have 
 *   been completed.
 * @author ArnyminerZ
 * @version 1 2020/10/05
 */
void pref_close()
{
    preferences.end();
    _prefOpen = false;
}

/**
 * Checks if the preference storage can be used.
 * @author ArnyminerZ
 * @version 1 2020/10/05
 * @return {bool} If the preferences are accessible.
 */
bool pref_available()
{
    bool prefAvailable = pref_open();
    if (prefAvailable)
        pref_close();
    return prefAvailable;
}

/**
 * Stores a String with key *key.
 * @param key The key to store
 * @param value The value to set
 * @author ArnyminerZ
 * @version 1 2020/10/05
 * @return {bool} If the storage was made successfully
 */
bool pref_setString(const char *key, String value)
{
    if (!pref_open())
        return false;

    preferences.putString(key, value);

    pref_close();

    return true;
}

/**
 * Stores a Boolean with key *key.
 * @param key The key to store
 * @param value The value to set
 * @author ArnyminerZ
 * @version 1 2020/10/05
 * @return {bool} If the storage was made successfully
 */
bool pref_setBool(const char *key, bool value)
{
    if (!pref_open())
        return false;

    preferences.putBool(key, value);

    pref_close();

    return true;
}

/**
 * Stores an integer with key *key.
 * @param key The key to store
 * @param value The value to set
 * @author ArnyminerZ
 * @version 1 2020/11/06
 * @return {bool} If the storage was made successfully
 */
bool pref_setInt(const char *key, int value)
{
    if (!pref_open())
        return false;

    preferences.putInt(key, value);

    pref_close();

    return true;
}

/**
 * Stores an unsigned char with key *key.
 * @param key The key to store
 * @param value The value to set
 * @author ArnyminerZ
 * @version 1 2020/11/06
 * @return {bool} If the storage was made successfully
 */
bool pref_setUChar(const char *key, unsigned char value)
{
    if (!pref_open())
        return false;

    preferences.putUChar(key, value);

    pref_close();

    return true;
}

/**
 * Stores an unsigned char array with key *key.
 * @param key The key to store
 * @param value The value to set
 * @param size The length of the array to store
 * @author ArnyminerZ
 * @version 1 2020/11/06
 * @return {bool} If the storage was made successfully
 */
bool pref_setUChar(const char *key, uint8_t *value, uint16_t size)
{
    if (!pref_open())
        return false;

    for (uint16_t c = 0; c < size; c++)
    {
        string k(key);
        k += c;
        char ka[k.length() + 1];
        strcpy(ka, k.c_str());
        preferences.putUChar(ka, value[c]);
    }

    pref_close();

    return true;
}

/**
 * Gets a String from the preferences storage.
 * @param key The key to get
 * @param defaultValue The default value if the key is not found
 * @param storeDefault If the default value should be stored if pref doesn't exist.
 * @author ArnyminerZ
 * @version 1 2020/10/05
 * @return {String} The read value from the preference storage
 */
String pref_getString(const char *key, String defaultValue = String(), bool storeDefault = false)
{
    if (!pref_open())
        return defaultValue;

    String result = preferences.getString(key, "N/A");

    if (result == "N/A")
    {
        result = defaultValue;
        if (storeDefault)
            pref_setString(key, defaultValue);
    }

    pref_close();

    return result;
}

/**
 * Gets a Boolean from the preferences storage.
 * @param key The key to get
 * @param defaultValue The default value if the key is not found
 * @author ArnyminerZ
 * @version 1 2020/10/05
 * @return {String} The read value from the preference storage
 */
bool pref_getBool(const char *key, bool defaultValue = false)
{
    if (!pref_open())
        return defaultValue;

    bool result = preferences.getBool(key, defaultValue);

    pref_close();

    return result;
}

/**
 * Gets an Integer from the preferences storage.
 * @param key The key to get
 * @param defaultValue The default value if the key is not found
 * @author ArnyminerZ
 * @version 1 2020/11/06
 * @return {Int} The read value from the preference storage
 */
uint32_t pref_getInt(const char *key, int defaultValue = -1)
{
    if (!pref_open())
        return defaultValue;

    int result = preferences.getInt(key, defaultValue);

    pref_close();

    return result;
}

/**
 * Gets an unsigned char from the preferences storage.
 * @param key The key to get
 * @param defaultValue The default value if the key is not found
 * @author ArnyminerZ
 * @version 1 2020/11/06
 * @return {Int} The read value from the preference storage
 */
uint32_t pref_getUChar(const char *key, uint8_t defaultValue = -1)
{
    if (!pref_open())
        return defaultValue;

    int result = preferences.getUChar(key, defaultValue);

    pref_close();

    return result;
}

/**
 * Gets an unsigned char array from the preferences storage.
 * @param key The key to get
 * @param size the size of the array
 * @author ArnyminerZ
 * @version 1 2020/11/06
 */
uint8_t *pref_getUCharArray(const char *key, int size)
{
    uint8_t *builder = new uint8_t[size + 1];

    if (!pref_open())
        return builder;

    for (int c = 0; c < size; c++)
    {
        string k(key);
        k += c;
        char ka[k.length() + 1];
        strcpy(ka, k.c_str());
        builder[c] = preferences.getUChar(ka);
    }

    pref_close();

    return builder;
}

void pref_del(const char* key){
    if (!pref_open())
        return;

    preferences.remove(key);

    pref_close();
}

/**
 * Removes the specified char array from the preferences storage.
 * @param key The key to set
 * @param size The size of the array
 * @author ArnyminerZ
 * @version 2020/11/06
 */
void pref_delUCharArray(const char* key, int size){
    if (!pref_open())
        return;

    for (int c = 0; c < size; c++)
    {
        string k(key);
        k += c;
        char ka[k.length() + 1];
        strcpy(ka, k.c_str());
        preferences.remove(ka);
    }
    
    pref_close();
}

#endif