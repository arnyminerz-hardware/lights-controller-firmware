/**
 * This file contains all the instructions for using the ESP32's BLE capabilities
 * 
 * @file ble.h
 * @author ArnyminerZ
 * @version 2020/11/07
 */

#ifndef BLE_H
#define BLE_H

// Include some libraries
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <BLE2902.h>
#include <string>

// Include some configuration files
#include "static/pref.h"
#include "static/consts.h"

// And some utils
#include "helper/preferences.h"

// Choose the core to use
#if CONFIG_FREERTOS_UNICORE
#define BLE_CORE 0
#else
#define BLE_CORE 1
#endif

using namespace std;

uint8_t bleSliderValues[SLIDER_COUNT];
bool _bleDebugNotify = false;

bool bleDeviceConnected = false;
bool bleOldDeviceConnected = false;
BLEServer *pServer = NULL;

BLECharacteristic *commCharacteristic;

/**
 * Gives an answer through the ble characteristic
 * 
 * @param pCharacteristic The characteristic to update
 * @param value The value to set
 * @param notify If a notify should be ran
 * @param debug If the sent answer should be shown through Serial
 * 
 * @author ArnyminerZ
 * @version 2020/11/10
 */
void ble_answer(BLECharacteristic *pCharacteristic, std::__cxx11::string value, bool notify = true, bool debug = true)
{
    if (notify)
        _bleDebugNotify = debug;
    pCharacteristic->setValue(value);
    if (notify)
        pCharacteristic->notify();
}

class BleServerCallbacks : public BLEServerCallbacks
{
    void onConnect(BLEServer *pServer)
    {
        bleDeviceConnected = true;
    };

    void onDisconnect(BLEServer *pServer)
    {
        bleDeviceConnected = false;
    }
};

class SetCharacteristicCallbacks : public BLECharacteristicCallbacks
{
    void onWrite(BLECharacteristic *pCharacteristic)
    {
        string value = pCharacteristic->getValue();
        String fullCommand = String(value.c_str());

        Serial.print("[BLE] < ");
        Serial.println(fullCommand);

        int hyphenIndex = fullCommand.indexOf('-');
        String commandName = fullCommand.substring(0, hyphenIndex);
        if (commandName.equalsIgnoreCase("SCONFIG"))
        {
            int secondHyphenIndex = fullCommand.indexOf('-', hyphenIndex + 1);
            String arg1 = fullCommand.substring(hyphenIndex + 1, secondHyphenIndex);
            String arg2 = fullCommand.substring(secondHyphenIndex + 1);
            Serial.println("Got command SCONFIG. arg1=" + arg1 + " arg2=" + arg2);
            ble_answer(pCharacteristic, "OK");
        }
        else
            ble_answer(pCharacteristic, "IC");
    }

    void onNotify(BLECharacteristic *pCharacteristic)
    {
        if (!_bleDebugNotify)
            return;
        Serial.print("[BLE] > ");
        Serial.println(pCharacteristic->getValue().c_str());
    }
};

void bleTask(void *params);

/**
 * Initializes the BLE service
 * 
 * @author ArnyminerZ
 * @version 2020/11/07
 */
void ble_begin()
{
    Serial.println();
    Serial.println("  Getting BLE name...");
    String prefBleName = pref_getString(PREF_BLE_NAME, PREF_BLE_NAME_DEFAULT, true);

    Serial.println("    Converting...");
    char ble[prefBleName.length() + 1];
    prefBleName.toCharArray(ble, prefBleName.length() + 1);

    Serial.print("    Name: ");
    Serial.println(ble);
    BLEDevice::init(ble);

    Serial.println("  Creating BLE server...");
    pServer = BLEDevice::createServer();
    Serial.println("    Adding callbacks...");
    pServer->setCallbacks(new BleServerCallbacks());

    Serial.println("  Creating BLE service...");
    BLEService *pService = pServer->createService(BLE_SERVICE_UUID);

    Serial.println("    Creating characteristic...");
    commCharacteristic = pService->createCharacteristic(
        BLE_CHARACT_COMM_UUID,
        BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE | BLECharacteristic::PROPERTY_NOTIFY | BLECharacteristic::PROPERTY_INDICATE);
    Serial.println("      Setting descriptor...");
    commCharacteristic->addDescriptor(new BLE2902());
    Serial.println("      Setting callback...");
    commCharacteristic->setCallbacks(new SetCharacteristicCallbacks());
    Serial.println("      Setting value...");
    commCharacteristic->setValue("");

    Serial.println("  Starting BLE service...");
    pService->start();

    Serial.println("  Advertising...");
    BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(BLE_SERVICE_UUID);
    pAdvertising->setScanResponse(false);
    pAdvertising->setMinPreferred(0x06); // functions that help with iPhone connections issue
    pAdvertising->setMinPreferred(0x12);
    BLEDevice::startAdvertising();

    Serial.println("  Pinning task...");
    xTaskCreatePinnedToCore(bleTask, BLE_TASK, 6144, NULL, 1, NULL, BLE_CORE);
}

void bleTask(void *params)
{
    while (true)
    {
        // notify changed value
        if (bleDeviceConnected)
            delay(1000); // bluetooth stack will go into congestion, if too many packets are sent, in 6 hours test i was able to go as low as 3ms

        // disconnecting
        if (!bleDeviceConnected && bleOldDeviceConnected)
        {
            delay(500);                  // give the bluetooth stack the chance to get things ready
            pServer->startAdvertising(); // restart advertising
            Serial.println("[¡] BLE Start advertising");
            bleOldDeviceConnected = bleDeviceConnected;
        }
        // connecting
        if (bleDeviceConnected && !bleOldDeviceConnected)
            // do stuff here on connecting
            bleOldDeviceConnected = bleDeviceConnected;
    }
}

#endif