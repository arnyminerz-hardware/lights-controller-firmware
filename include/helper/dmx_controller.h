/**
 * The helper functions for controlling all the
 *   DMX stuff.
 */

#ifndef DMX_CONTROLLER_H
#define DMX_CONTROLLER_H

#include <ESPDMX.h>

DMXESPSerial dmx;

/**
 * Initializes the DMX controller
 * 
 * @author ArnyminerZ
 * @version 1 20201029
 */
bool dmx_begin()
{
    dmx.init();
    return true;
}

/**
 * Sends a DMX value through a channel. Submits the content automatically
 * 
 * @author ArnyminerZ
 * @version 1 20201029
 */
void dmx_send(short channel, uint8_t value)
{
    dmx.write(channel, value);
    dmx.update();
}

#endif