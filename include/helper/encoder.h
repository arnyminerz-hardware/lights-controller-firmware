/**
 * The file for doing all the Encoder processing
 * 
 * @file encoder.h
 * @author ArnyminerZ
 * @version 2020/11/07
 */

#ifndef ENCODER_H
#define ENCODER_H

// Include some required libraries
#include <ESP32Encoder.h>

// Include some configuration files
#include "static/consts.h"

// Declare the encoder object
ESP32Encoder encoder;

uint32_t encoderValue = 0;


/**
 * Initializes the encoder
 * 
 * @author ArnyminerZ
 * @version 2020/11/07
 */
void encoder_begin(){
    // Enable the internal pullup resistors
    ESP32Encoder::useInternalWeakPullResistors=UP;

    // Set the pins
    encoder.attachHalfQuad(ENCODER_A, ENCODER_B);

    // Clear the count
    encoder.clearCount();
}

void encoder_read(){
    encoderValue = (int32_t)encoder.getCount();
}


#endif