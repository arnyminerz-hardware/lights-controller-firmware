/**
 * All the input-output related functions
 * 
 * @file io.h
 * @author ArnyminerZ
 * @version 2020/11/07
 */

#ifndef IO_H
#define IO_H

// Choose the core to use
#if CONFIG_FREERTOS_UNICORE
#define IO_CORE 0
#else
#define IO_CORE 1
#endif

// The Arduino library is required
#include <Arduino.h>

// Include some configuration files
#include "static/consts.h"

#include "helper/ble.h"

unsigned long ioReadAccumulator;

void inputsReadTask(void *params);

/**
 * Initializes all the inputs
 */
void io_inputs_begin()
{
    analogSetAttenuation(ANALOG_READ_ATTENUATION);
    analogReadResolution(ANALOG_READ_RESOLUTION);

    pinMode(SLIDER_1_PIN, INPUT);
    pinMode(SLIDER_2_PIN, INPUT);
    pinMode(SLIDER_3_PIN, INPUT);
    pinMode(SLIDER_4_PIN, INPUT);
    pinMode(SLIDER_5_PIN, INPUT);

    xTaskCreatePinnedToCore(inputsReadTask, IO_TASK, 6144, NULL, 1, NULL, IO_CORE);
}

/**
 * Initializes all the outputs
 */
void io_outputs_begin()
{
    pinMode(LED_BUILTIN, OUTPUT);
}

/**
 * Turns on or off the built-in LED
 * 
 * @param enabled If the LED should be turned on
 * 
 * @author ArnyminerZ
 * @version 2020/11/07
 */
void io_status(bool enabled)
{
    digitalWrite(LED_BUILTIN, enabled);
}

void inputsReadTask(void *params)
{
    for (;;)
    {
        for (int i = 0; i < SLIDER_COUNT; i++)
        {
            ioReadAccumulator = 0;
            for (int c = 0; c < ANALOG_READ_COUNT; c++)
            {
                ioReadAccumulator += analogRead(SLIDER_PINS[i]);
                delay(ANALOG_READ_DELAY);
            }

            bleSliderValues[i] = ioReadAccumulator / ANALOG_READ_COUNT;
        }
    }
}

#endif