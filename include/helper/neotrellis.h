/**
 * The helper functions for controlling the Neotrellis board
 */

#ifndef NEOTRELLIS_H
#define NEOTRELLIS_H

#if CONFIG_FREERTOS_UNICORE
#define TRELLIS_CORE 0
#else
#define TRELLIS_CORE 1
#endif

#include "static/consts.h"

#include <Adafruit_NeoTrellis.h>

#ifndef DISABLE_NEOTRELLIS

//create a matrix of trellis panels
Adafruit_NeoTrellis t_array[NEOTRELLIS_Y_DIM / 4][NEOTRELLIS_X_DIM / 4] = {

    {Adafruit_NeoTrellis(NEOTRELLIS_BOARD_1_ADDRESS)},
    {Adafruit_NeoTrellis(NEOTRELLIS_BOARD_2_ADDRESS)}

};
Adafruit_MultiTrellis trellis((Adafruit_NeoTrellis *)t_array, NEOTRELLIS_Y_DIM / 4, NEOTRELLIS_X_DIM / 4);

uint8_t trellisStatus[NEOTRELLIS_COUNT][3];
#endif

#define NEOTRELLIS_RESULT_OK 0
#define NEOTRELLIS_RESULT_ERROR 1
#define NEOTRELLIS_RESULT_DISABLED 2

void neotrellisTask(void *params);

/**
 * Starts the neotrellis
 * @author ArnyminerZ
 * @version 1 2020/10/05
 * @return 0 if the trellis was initialized, 1 if the trellis could not be initialized, 2 if the trellis is disabled
 */
uint8_t trellis_begin()
{
    // TODO: Disable Neotrellis through pref so it can be reenabled without updating
#ifdef DISABLE_NEOTRELLIS
    return 2;
#else
    bool started = trellis.begin();

    if (started)
        xTaskCreatePinnedToCore(neotrellisTask, NEOTRELLIS_TASK, 6144, NULL, 1, NULL, TRELLIS_CORE);

    return started ? 0 : 1;
#endif
}

/**
 * Sets the color of a single trellis button
 * @author ArnyminerZ
 * @version 1 2020/10/05
 * @param index The position of the button
 * @param r The red value of the color
 * @param g The green value of the color
 * @param b The blue value of the color
 * @param shouldShow If the set color should be applied
 */
void trellis_paint(int index, uint8_t r, uint8_t g, uint8_t b, bool shouldShow = true)
{
#ifndef DISABLE_NEOTRELLIS
    trellis.setPixelColor(index, seesaw_NeoPixel::Color(r, g, b));
    if (shouldShow)
        trellis.show();
#endif
}

/**
 * Sets the color of a single trellis button
 * @author ArnyminerZ
 * @version 1 2020/10/05
 * @param x The x position of the button
 * @param y The y position of the button
 * @param r The red value of the color
 * @param g The green value of the color
 * @param b The blue value of the color
 * @param shouldShow If the set color should be applied
 */
void trellis_paint(short x, short y, uint8_t r, uint8_t g, uint8_t b, bool shouldShow = true)
{
    trellis_paint(NEOTRELLIS_X_DIM * y + x, r, g, b, shouldShow);
}

/**
 * Fills a region of the trellis with a specific color
 * @author ArnyminerZ
 * @version 1 2020/10/05
 * @param from_x The starting x position of the area to fill
 * @param from_y The starting y position of the area to fill
 * @param to_x The ending x position of the area to fill
 * @param to_y The ending y position of the area to fill
 * @param r The red value of the color
 * @param g The green value of the color
 * @param b The blue value of the color
 */
void trellis_fill(short from_x, short from_y, short to_x, short to_y, uint8_t r, uint8_t g, uint8_t b)
{
#ifndef DISABLE_NEOTRELLIS
    short width = to_x - from_x;
    short height = to_y - from_y;
    for (int c = 0; c < width * height; c++)
        trellis_paint(c, r, g, b, false);

    trellis.show();
#endif
}

/**
 * Adds a listener to a specific trellis button
 * @author ArnyminerZ
 * @version 1 2020/10/05
 * @param x The x position of the button
 * @param y The y position of the button
 * @param onPress If the detection should be made when the button is pressed
 * @param onRelease If the detection should be made when the button is released
 * @param callback The function to call when an action is made
 */
void trellis_addListener(short x, short y, bool onPress, bool onRelease, TrellisCallback (*callback)(keyEvent))
{
#ifndef DISABLE_NEOTRELLIS
    if (!onPress && !onRelease)
        return;

    if (onPress)
        trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_RISING, true);
    if (onRelease)
        trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_FALLING, true);
    trellis.registerCallback(x, y, callback);
#endif
}

/**
 * Adds a listener to a specific trellis button
 * @author ArnyminerZ
 * @version 1 2020/10/05
 * @param x The x position of the button
 * @param y The y position of the button
 * @param onPress If the detection should be made when the button is pressed
 * @param onRelease If the detection should be made when the button is released
 * @param callback The function to call when an action is made
 */
void trellis_addListener(short index, bool onPress, bool onRelease, TrellisCallback (*callback)(keyEvent))
{
#ifndef DISABLE_NEOTRELLIS
    if (!onPress && !onRelease)
        return;

    if (onPress)
        trellis.activateKey(index, SEESAW_KEYPAD_EDGE_RISING, true);
    if (onRelease)
        trellis.activateKey(index, SEESAW_KEYPAD_EDGE_FALLING, true);
    trellis.registerCallback(index, callback);
#endif
}

/**
 * Adds a listener to a range of trellis buttons
 * @author ArnyminerZ
 * @version 1 2020/10/05
 * @param from_x The starting x position of the button
 * @param from_y The starting y position of the button
 * @param to_x The ending x position of the button
 * @param to_y The ending y position of the button
 * @param onPress If the detection should be made when the button is pressed
 * @param onRelease If the detection should be made when the button is released
 * @param callback The function to call when an action is made
 */
void trellis_addListener(short from_x, short from_y, short to_x, short to_y, bool onPress, bool onRelease, TrellisCallback (*callback)(keyEvent))
{
#ifndef DISABLE_NEOTRELLIS
    if (!onPress && !onRelease)
        return;

    short width = to_x - from_x;
    short height = to_y - from_y;
    for (int c = 0; c < width * height; c++)
        trellis_addListener(c, onPress, onRelease, callback);
#endif
}

void neotrellisTask(void *params)
{
#ifndef DISABLE_NEOTRELLIS
    for (;;)
        trellis.read();
#endif
}

#endif