/**
 * A helper so the LED 14 segment backpack is easier to control
 * 
 * @file backpack.h
 * @author ArnyminerZ
 * @version 2020/11/06
 */

#ifndef BACKPACK_H
#define BACKPACK_H

// Import the required libraries
#include <Wire.h>
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"

// Import the required config files
#include "static/consts.h"

Adafruit_AlphaNum4 alpha4 = Adafruit_AlphaNum4();

/**
 * Initializes the LED backpack
 * 
 * @author ArnyminerZ
 * @version 2020/11/06
 */
void backpack_begin()
{
    alpha4.begin(BACKPACK_ADDRESS);
}

/**
 * Writes some content into the backpack
 * 
 * @param text The text to write. Max 4 chars
 * 
 * @author ArnyminerZ
 * @version 2020/11/06
 */
void backpack_write(String text)
{
    for (int c = 0; c < 4; c++) // Iterates through the first 4 characters
        if (text.length() > c)
            alpha4.writeDigitAscii(text.charAt(c), c);
        else
            break; // If the string is shorter, cut the loop

    alpha4.writeDisplay();
}

#endif