/**
 * All the code for the DMX communication
 * 
 * @file dmx.h
 * @author ArnyminerZ
 * @version 2020/11/07
 */

#ifndef DMX_H
#define DMX_H

// Include some libraries
#include <ESPDMX.h>

// Include some configuration files
#include "static/consts.h"

// Declare the source objects
DMXESPSerial dmx;

uint8_t channels[512];

/**
 * Initializes the DMX communicator
 */
void dmx_begin()
{
    dmx.init(512, DMX_OUTPUT);
}

/**
 * Sends a new update to the DMX
 */
void dmx_write(int channel, uint8_t value, bool update = true)
{
    dmx.write(channel, value);
    if (update)
        dmx.update();
}

#endif