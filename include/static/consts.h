#ifndef CONSTS_H
#define CONSTS_H

#define PREF_NEOTRELLIS_DETECTED "neotr_setup"

#define PREF_BLE_NAME "ble_name"
#define AVAILABLE_PREFS_COUNT 2
const char *AVAILABLE_PREFS[AVAILABLE_PREFS_COUNT] = { PREF_NEOTRELLIS_DETECTED, PREF_BLE_NAME };


//#define DISABLE_NEOTRELLIS // Uncomment to disable Neotrellis

// How much time will the user have to test all the neotrellis buttons
#define NEOTRELLIS_TEST_TIMEOUT 60000
// How often should it be test if all the buttons have been pressed
#define NEOTRELLIS_TEST_TICKER 20


#define ANALOG_READ_ATTENUATION ADC_6db
#define ANALOG_READ_RESOLUTION 11
#define ANALOG_READ_MAX 2 ^ ANALOG_READ_RESOLUTION
#define ANALOG_READ_COUNT 50 // How much iterations
#define ANALOG_READ_DELAY 3  // The time delay between different readings


#define DMX_OUTPUT 5

#define SLIDER_1_PIN 4
#define SLIDER_2_PIN 15
#define SLIDER_3_PIN 34
#define SLIDER_4_PIN 35
#define SLIDER_5_PIN 27
#define SLIDER_COUNT 5
const unsigned short SLIDER_PINS[SLIDER_COUNT] = {SLIDER_1_PIN, SLIDER_2_PIN, SLIDER_3_PIN, SLIDER_4_PIN, SLIDER_5_PIN};

#define NEOTRELLIS_BOARD_INT 14

#define ENCODER_BUTTON 26
#define ENCODER_A 12
#define ENCODER_B 13

#define NEOTRELLIS_BOARD_1_ADDRESS 0x33 // A0+A2
#define NEOTRELLIS_BOARD_2_ADDRESS 0x31 // A0+A1

#define NEOTRELLIS_X_DIM 4 // The amount of neotrellis buttons horizontally
#define NEOTRELLIS_Y_DIM 8 // The amount of neotrellis buttons vertically
#define NEOTRELLIS_COUNT NEOTRELLIS_X_DIM *NEOTRELLIS_Y_DIM

#define BACKPACK_ADDRESS 0x70

#define BLE_SERVICE_UUID "04d4f13b-e9b9-4f0c-8f91-416d22daffcc"
#define BLE_CHARACT_COMM_UUID "f367a7f1-51d9-4621-acd9-e4e9628f9b56"

#endif