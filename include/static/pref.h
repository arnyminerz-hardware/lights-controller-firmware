/**
 * This file is intended to contain some configuration that don't match
 *   the consts.h considerations.
 */

#ifndef PREF_H
#define PREF_H
#include <Arduino.h>

#define VERSION "1.0.0"
#define WATERMARK "© 2020 ArnyminerZ - https://arnyminerz.com"


#define BAUD_RATE 115200


// The namespace to store all the memory that should be kept
//   when the device turns off.
// Note: Should have at most 15 characters
#define PREF_NAMESPACE "lc-001"

#define PREF_BLE_NAME_DEFAULT "Lights Controller"


#define BLE_TASK "ble"


#define IO_TASK "IO"


#define NEOTRELLIS_TASK "trellis"

#endif