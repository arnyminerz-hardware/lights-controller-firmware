/**
 * This file is used for modifying the source code without
 *   removing or commenting any code, it'll be done directly
 *   through the compiler.
 * 
 * @file forcer.h
 * @author ArnyminerZ
 * @version 2020/11/06
 * @copyright 2020 - ArnyminerZ
 * @license AGPL-3.0
 */

/**
 * To enable an option, uncomment it.
 */

//#define ENABLE_TEST_NEOTRELLIS // If the Neotrellis individual test should be ran

#ifdef ENABLE_TEST_NEOTRELLIS
/**
 * ??? Configuration for ENABLE_TEST_NEOTRELLIS
 */
//#define NEOTRELLIS_SINGLE // Uncomment to select single-board test
#define NEOTRELLIS_MULTIPLE // Uncomment to select single-board test

#define NEOTRELLIS_BOARD_1_ADDRESS 0x33 // The first Neotrellis board address. Default: 0x33 (A0+A2)
#define NEOTRELLIS_BOARD_2_ADDRESS 0x31 // The second Neotrellis board address. Default: 0x31 (A0+A1)

#define NEOTRELLIS_BOARD_ADDRESS NEOTRELLIS_BOARD_2_ADDRESS // The Neotrellis board for the single test
#endif


/**
 * !!! Do not modify below here !!!
 */
#ifdef ENABLE_TEST_NEOTRELLIS
#define SRC_FORCED

#if defined(NEOTRELLIS_SINGLE) && defined(NEOTRELLIS_MULTIPLE)
#error "Please, select only NEOTRELLIS_SINGLE or NEOTRELLIS_MULTIPLE not both."
#else
#warning "Forcing source code to use Neotrellis example"

#ifdef NEOTRELLIS_SINGLE
#include "../.libraries/Adafruit_Seesaw/examples/NeoTrellis/basic/basic.cpp"
#endif

#ifdef NEOTRELLIS_MULTIPLE
#include "../.libraries/Adafruit_Seesaw/examples/NeoTrellis/multitrellis/basic/basic.cpp"
#endif
#endif
#endif