# Design Flaws for V1
1. The Micro-USB connector of the ESP32 Devkit board can't be accessed, the I2C connectors hide it.
2. `ADC2` can't be used at the same time than WiFi.

# Neotrellis
## Voltage Regulator
The Neotrellis boards have an internal voltage regulator named `AP2112-3.3`, its datasheet can be found inside `./datasheets`.

It has a recommended supply voltage of between `2.5` and `6.0` volts, so it has to be taken into account that the boards can't be powered directly with the Power Supply.