/**
 * This is the firmware file for the Lights Controller system.
 * The code was first made by Arnau Mora aka ArnyminerZ.
 * All the corresponding editors and contributors may be added.
 * 
 * The following code, the documentation and everything in this repository 
 *   is under the AGPL-v3.0 license (https://choosealicense.com/licenses/agpl-3.0/).
 * 
 * The most unclear functions have been documented, but messages shown
 *   with Serial.print* will be considered as comments.
 * 
 * @file main.cpp
 * @author ArnyminerZ
 * @copyright 2020 - ArnyminerZ
 * @license AGPL-3.0
 */

#include <Arduino.h>

#include "forcer.h"

#ifndef SRC_FORCED // Do not do anything if the source code is forced. (View forcer.h)

// Include some static files
#include "static/consts.h"
#include "static/pref.h"

// Include some helper libraries, to make code more readable
#include "helper/preferences.h"
#include "helper/system.h"
#include "helper/neotrellis.h"
//#include "helper/backpack.h"
#include "helper/io.h"
#include "helper/ble.h"
#include "helper/dmx.h"
#include "helper/encoder.h"

// Include the listeners
#include "test/neotrellis/listeners.h"

void setup()
{
  Serial.begin(BAUD_RATE);

  Serial.println("> Booting up Lights Controller...");
  Serial.println(VERSION);
  Serial.println(WATERMARK);
  Serial.println();

  pinMode(LED_BUILTIN, OUTPUT);

  //Serial.print("> Initializing Backpack...");
  //backpack_begin();
  //Serial.println("ok");

  //backpack_write("BOOT");

  Serial.print("> Checking preference access...");
  if (pref_available())
    Serial.println("ok");
  else
  {
    Serial.println("error!");
    Serial.println();
    Serial.println("¡ Preferences can't be accessed, this is a critical error.");
    Serial.println("  System will be blocked. Watchdog may restart the board");
    Serial.println("    automatically.");
    while (1)
      ;
  }

  Serial.print("> Initializing inputs...");
  io_inputs_begin();
  Serial.println("ok");

  Serial.print("> Initializing outputs...");
  io_outputs_begin();
  Serial.println("ok");

  Serial.print("> Initializing encoder...");
  encoder_begin();
  Serial.println("ok");

  Serial.print("> Initializing DMX...");
  dmx_begin();
  Serial.println("ok");

  Serial.print("> Initializing Neotrellis...");
  uint8_t trellisStarted = trellis_begin();
  if (trellisStarted == NEOTRELLIS_RESULT_ERROR)
  {
    Serial.println("error!");
    Serial.println("¡ There was an error while starting the Neotrellis, this is a critical error.");
    Serial.println("  System will be blocked. Watchdog may restart the board");
    Serial.println("    automatically.");
    while (1)
      ;
  }
  else if (trellisStarted == NEOTRELLIS_RESULT_DISABLED)
  {
    Serial.println("skip!");
    Serial.println("¡ Neotrellis is disabled, to enable, comment out DISABLE_NEOTRELLIS in consts.h");
  }
  else
  {
    Serial.println("ok");

    Serial.print("> Painting blue...");
    trellis_fill(0, 0, NEOTRELLIS_X_DIM, NEOTRELLIS_Y_DIM, 0, 0, 255);
    Serial.println("ok!");

    bool neotrellisDetected = pref_getBool(PREF_NEOTRELLIS_DETECTED, false);
    if (!neotrellisDetected)
    {
      Serial.println("> The system doesn't know if Neotrellis is actually working.");
      Serial.println("  Will execute a diagnostic test");

      pressedKeys = 0;

      Serial.print("> Registering listeners...");
      trellis_addListener(0, 0, NEOTRELLIS_X_DIM, NEOTRELLIS_Y_DIM, true, false, check_press);
      Serial.println("ok");

      Serial.print("> Filling board red...");
      trellis_fill(0, 0, NEOTRELLIS_X_DIM, NEOTRELLIS_Y_DIM, 255, 0, 0);
      Serial.println("ok");

      Serial.println("> Please, press all the buttons");
      Serial.print("> You have ");
      Serial.print(NEOTRELLIS_TEST_TIMEOUT);
      Serial.print("ms...");

      unsigned long startTime = millis();
      while (true)
      {
        if (millis() - startTime >= NEOTRELLIS_TEST_TIMEOUT)
        {
          Serial.println("error!");
          Serial.println("¡ There was an error while checking for the Neotrellis, this is a critical error.");
          Serial.println("  System will be blocked. Watchdog may restart the board");
          Serial.println("    automatically.");
          while (true)
            ;
        }

        if (pressedKeys >= NEOTRELLIS_COUNT)
        {
          Serial.println("ok!");
          break;
        }

        delay(NEOTRELLIS_TEST_TICKER);
      }

      pref_setBool(PREF_NEOTRELLIS_DETECTED, true);
    }

    trellis_addListener(0, 0, NEOTRELLIS_X_DIM, NEOTRELLIS_Y_DIM, true, true, trellis_routine);

    for (int c = 0; c < NEOTRELLIS_COUNT; c++)
    {
      trellis_paint(c, 0, 0, 0, true);
      delay(10);
    }
  }

  Serial.print("> Initializing BLE...");
  ble_begin();
  Serial.println("ok");

  //backpack_write("HELO");
}

void loop()
{
  encoder_read();
  delay(100); // Delay a bit so the watchdog doesn't get called
}
#endif