let links;

const initToolbar = () => {
  const toolbar = document.getElementsByClassName("toolbar")[0];
  const container = document.getElementsByClassName("container")[0];
  links = toolbar.getElementsByTagName("a");
  const panels = container.getElementsByTagName("div");

  if (links.length <= 0 || panels.length <= 0)
    return console.error(
      "Could not init toolbar. Links:",
      links,
      "Panels:",
      panels
    ); // just in case no a or panels was found

  const activateItem = (index) => {
    for (let c = 0; c < links.length; c++) {
      const link = links[c];
      link.classList.remove("active");
    }
    for (let c = 0; c < panels.length; c++) {
      const panel = panels[c];
      panel.classList.add("d-none");
    }

    links[index].classList.add("active");
    panels[index].classList.remove("d-none");
  };

  for (let c = 0; c < links.length; c++) {
    const link = links[c];
    link.addEventListener("click", () => {
      activateItem(c);
    });
  }
  activateItem(0);
};
